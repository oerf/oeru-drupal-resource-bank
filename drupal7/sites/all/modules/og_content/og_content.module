<?php
/**
 * @file
 * Code for the OG Content Administration module.
 */

/**
 * Implements hook_og_permission().
 */
function og_content_og_permission() {
  $permissions = array(
    'manage_group_contents' => array(
      'title' => t('Manage group contents'),
    ),
    'configure og_content menu' => array(
      'title' => t('Configure Group Menu links'),
    ),
  );

  return $permissions;
}

/**
 * Implements hook_menu().
 */
function og_content_menu() {
  $items = array();

  $items['group/%/%/admin/content'] = array(
    'title' => 'Content',
    'page callback' => 'og_content_content_page',
    'page arguments' => array(1, 2),
    'access callback' => 'og_user_access',
    'access arguments' => array(1, 2, 'manage_group_contents'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'og_content.pages.inc',
  );

  $items['group/%og_content/%og_content/admin/content/add'] = array(
    'title' => 'Add content',
    'page callback' => 'og_content_content_add_page',
    'page arguments' => array(1, 2),
    'access callback' => 'og_content_access',
    'access arguments' => array(1, 2, 'add content'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'og_content.pages.inc',
  );

  // Get all available group content types.
  foreach (og_content_get_available_node_types() as $type) {
    $type_url_str = str_replace('_', '-', $type->type);
    $items['group/%og_content/%og_content/admin/content/add/' . $type_url_str] = array(
      'title' => $type->name,
      'title callback' => 'check_plain',
      'page callback' => 'node_add',
      'page arguments' => array($type->type),
      'access callback' => 'og_user_access',
      'access arguments' => array(1, 2, "create $type->type content"),
      'description' => $type->description,
      'file path' => drupal_get_path('module', 'node'),
      'file' => 'node.pages.inc',
    );
  }

  $items['admin/config/group/og_menu/links'] = array(
    'title' => 'OG Menu Links',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('og_content_menu_links_settings_form'),
    'access arguments' => array('configure og_content menu'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'og_content.admin.inc',
  );

  return $items;
}

/**
 * Returns argument map from the current router item.
 */
function og_content_to_arg($arg, $map, $index) {
  $item = menu_get_item();
  return $item['map'][$index];
}

/**
 * Access callback.
 *
 * @see og_content_menu()
 */
function og_content_access($group_type, $gid, $op) {
  if ($op == 'add content') {
    $types = og_content_get_available_node_types();
    foreach ($types as $type => $node_type) {
      if (og_user_access($group_type, $gid, "create $type content")) {
        return TRUE;
      }
    }
  }

  return FALSE;
}

/**
 * Get a list of available "group content" node types.
 *
 * @return array
 */
function og_content_get_available_node_types() {
  $types = array();

  foreach (node_type_get_types() as $type) {
    if (og_is_group_content_type('node', $type->type)) {
      $types[$type->type] = $type;
    }
  }

  return $types;
}

/**
 * Implements hook_og_ui_get_group_admin()
 */
function og_content_og_ui_get_group_admin($group_type, $gid) {
  $items = array();
  $entity = entity_load_single($group_type, $gid);

  if (!empty($entity) && og_is_group($group_type, $entity)) {
    if (og_user_access($group_type, $gid, 'manage_group_contents')) {
      $items['content'] = array(
        'title' => t('Manage Content'),
        'description' => t('Manage contents of this group.'),
        // The final URL will be "group/$entity_type/$etid/admin/content".
        'href' => 'admin/content',
      );
    }

    if (og_content_access($group_type, $gid, 'add content')) {
      $items['add-content'] = array(
        'title' => t('Add content'),
        'description' => t('Add new content for this group'),
        // The final URL will be "group/$entity_type/$etid/admin/content/add".
        'href' => 'admin/content/add',
      );
    }
  }

  return $items;
}

/**
 * Implements hook_og_ui_get_group_admin_alter().
 */
function og_content_og_ui_get_group_admin_alter(&$data, $context) {
  $stored = variable_get('og_content_menu_links_weight', array());

  foreach ($data as $key => $item) {
    $enabled = isset($stored[$key]['enabled']) ? $stored[$key]['enabled'] : TRUE;

    if (!$enabled && !user_access('configure og_content menu')) {
      unset($data[$key]);
    }
    else {
      $data[$key]['title'] = !empty($stored[$key]['override_title']) ? t($stored[$key]['override_title']) : $data[$key]['title'];
      $data[$key]['weight'] = isset($stored[$key]['weight']) ? $stored[$key]['weight'] : 0;
    }
  }

  uasort($data, 'sort_og_menu_links');
}

/**
 * Implements hook_block_info().
 */
function og_content_block_info() {
  $blocks = array();

  $blocks['group_admin_menu'] = array(
    'info' => t('OG Content: Group admin menu'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function og_content_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'group_admin_menu':
      $og_context = og_context();
      $menu = '';

      if (!empty($og_context)) {
        module_load_include('inc', 'og_ui', 'og_ui.admin');
        $menu = og_content_admin_overview($og_context['group_type'], $og_context['gid']);
      }

      $block['subject'] = t('Administration Menu');
      $block['content'] = $menu;
      break;
  }

  return $block;
}

/**
 * Provide an overview of the administrator menu items.
 *
 * @param $entity_type
 *   The entity type.
 * @param $etid
 *   The entity ID.
 *
 * @return array
 *   A render array of the admin block content.
 */
function og_content_admin_overview($entity_type, $etid) {
  $content = array();
  $items = og_ui_get_group_admin($entity_type, $etid);

  if (empty($items)) {
    return $content;
  }

  $stored = variable_get('og_content_menu_links_weight', array());

  foreach ($items as $key => &$item) {
    $enabled = isset($stored[$key]['enabled']) ? $stored[$key]['enabled'] : TRUE;

    if (!$enabled) {
      unset($items[$key]);
    }
    else {
      // Re-format the URL.
      $item['href'] = "group/$entity_type/$etid/" . $item['href'];
      $item['localized_options'] = array();
      $item['weight'] = isset($stored[$key]['weight']) ? $stored[$key]['weight'] : 0;
    }
  }

  uasort($items, 'sort_og_menu_links');

  $content['admin_block_content'] = array(
    '#theme' => 'admin_block_content',
    '#content' => $items,
    '#access' => !empty($items),
  );

  return $content;
}

function sort_og_menu_links($a, $b) {
  if ($a['weight'] == $b['weight']) {
    return 0;
  }
  return ($a['weight'] < $b['weight']) ? -1 : 1;
}

/**
 * Implements hook_form_alter().
 */
function og_content_form_alter(&$form, &$form_state, $form_id) {
  $og_bundles = og_get_all_group_content_bundle();

  // Skip node forms that are not bound to group contents.
  if (empty($form['#node_edit_form']) || !isset($og_bundles[$form['#entity_type']][$form['#bundle']])) {
    return;
  }

  // Manage visibility of group vocabularies while creating a node from outside
  // a group.
  if (empty($form['nid']['#value']) && empty($_GET['og_group_ref']) && isset($form['og_vocabulary'])) {
    $form['og_vocabulary']['#access'] = FALSE;
  }

  // Move group vocabularies into the appropriate vertical tab.
  $group_tags = 'group_' . $form['#bundle'] . '_tags';

  if (isset($form['#groups'][$group_tags])) {
    $form['#groups'][$group_tags]->children[] = 'og_vocabulary';
    $form['#group_children']['og_vocabulary'] = $group_tags;
  }

  if (!empty($form['og_vocabulary'][LANGUAGE_NONE][0])) {
    foreach (element_children($form['og_vocabulary'][LANGUAGE_NONE][0]) as $vid) {
      $form['og_vocabulary'][LANGUAGE_NONE][0][$vid]['#title'] = preg_replace('|(\[.*?\] - )(.*?)|', '$2', $form['og_vocabulary'][LANGUAGE_NONE][0][$vid]['#title']);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function og_content_form_views_exposed_form_alter(&$form, &$form_state, $form_id) {
  if ($form_state['view']->name != 'og_content_contents') {
    return;
  }

  $bundles = array();
  $content_bundles = og_get_all_group_content_bundle();
  foreach ($content_bundles as $entity_type => $entity_bundles) {
    foreach (array_keys($entity_bundles) as $entity_bundle) {
      $bundles[$entity_bundle] = $entity_type;
    }
  }

  // Keep only group content types.
  foreach ($form['type']['#options'] as $type_name => $label) {
    if ($type_name == 'All') {
      continue;
    }

    if (empty($bundles[$type_name]) || !og_is_group_content_type($bundles[$type_name], $type_name)) {
      unset($form['type']['#options'][$type_name]);
    }
  }
}

/**
 * Implements hook_tokens_alter().
 */
function og_content_tokens_alter(array &$replacements, array $context) {
  if (($context['type'] == 'term') && !empty($replacements['[term:og-group-ref:0:url:path]']) && preg_match('/^node\/\d+/', $replacements['[term:og-group-ref:0:url:path]'])) {
    $replacements['[term:og-group-ref:0:url:path]'] = url($replacements['[term:og-group-ref:0:url:path]']);
  }
}

/**
 * Implements hook_views_api().
 */
function og_content_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_menu_alter().
 */
function og_content_menu_alter(&$items) {
  // Alter menu/add/* for all applicable node types.
  foreach (node_permissions_get_configured_types() as $type) {
    $type = str_replace('_', '-', $type);
    $items['node/add/' . $type]['access callback'] = '_og_content_node_access_callback';
  }
}

/**
 * Specialized node access callback for node/add/*.
 */
function _og_content_node_access_callback($op, $type) {
  return user_access("$op $type content");
}

/**
 * Implements hook_theme().
 */
function og_content_theme($existing, $type, $theme, $path) {
  return array(
    'og_content_menu_links_settings_form' => array(
      'render element' => 'form',
      'file' => 'og_content.admin.inc',
    ),
  );
}

